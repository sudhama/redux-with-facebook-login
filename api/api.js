import express from 'express';
import session from 'express-session';
import mongoose from 'mongoose';
import passport from 'passport';
import { Strategy } from 'passport-facebook';
import bodyParser from 'body-parser';
import config from '../src/config';
import * as actions from './actions/index';
import {mapUrl} from 'utils/url.js';
import PrettyError from 'pretty-error';
import http from 'http';
import SocketIo from 'socket.io';
import User from '../src/models/user'

const pretty = new PrettyError();
const app = express();

passport.serializeUser((user, cb) => {
  cb(null, user);
});

passport.deserializeUser((obj, cb) => {
  cb(null, obj);
});

passport.use(new Strategy({
    clientID: config.fb_client_id,
    clientSecret: config.fb_secret_key,
    callbackURL: config.fb_cb_url
  }, (accessToken, refreshToken, profile, done) => {
    console.log('new strategy method called');
    console.log('access token is' + accessToken);
    console.log('user name' + profile.displayName);
    console.log('user id' + profile.id);

    User.findOne({
        'facebook.id': profile.id 
    }, (err, user) => {
      if (err) {
        return done(err);
      }

      //No user was found... so create a new user with values from Facebook (all the profile. stuff)
      if (!user) {
          user = new User({
              name: profile.displayName,
              email: profile.emails[0].value,
              username: profile.username,
              provider: 'facebook',
              //now in the future searching on User.findOne({'facebook.id': profile.id } will match because of this next line
              facebook: profile._json
          });
          user.save((err) => {
              if (err) console.log(err);
              return done(err, user);
          });
          return done(null, profile);
      } else {
          //found user. Return
          return done(err, user);
      }

    })

  }
));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions


app.get('/facebooklogin', 
  passport.authenticate('facebook', { scope: ['user_friends', 'email', 'manage_pages', 'user_about_me']}));

app.get('/facebooklogin/return', 
  passport.authenticate(
    'facebook', 
    { failureRedirect: '/login'}
  ),
  (req, res) => {
    res.redirect('/');
  });

const server = new http.Server(app);


/**
* Database configuration
**/ 
const connectToDatabase = () => {
  mongoose.connect(config.database, (err, res) => {
    if(err) {
        console.log('Error connecting to: ' + config.database + '. ' + err);
      } else {
        console.log('Successfully connected to: ' + config.database);
      }
  });
}

connectToDatabase();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connectToDatabase);

const io = new SocketIo(server);
io.path('/ws');

app.use(session({
  secret: 'react and redux rule!!!!',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60000 }
}));
app.use(bodyParser.json());


app.use((req, res) => {
  const splittedUrlPath = req.url.split('?')[0].split('/').slice(1);

  const {action, params} = mapUrl(actions, splittedUrlPath);

  if (action) {
    action(req, params)
      .then((result) => {
        if (result instanceof Function) {
          result(res);
        } else {
          res.json(result);
        }
      }, (reason) => {
        if (reason && reason.redirect) {
          res.redirect(reason.redirect);
        } else {
          console.error('API ERROR:', pretty.render(reason));
          res.status(reason.status || 500).json(reason);
        }
      });
  } else {
    res.status(404).end('NOT FOUND');
  }
});


const bufferSize = 100;
const messageBuffer = new Array(bufferSize);
let messageIndex = 0;

if (config.apiPort) {
  const runnable = app.listen(config.apiPort, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> 🌎  API is running on port %s', config.apiPort);
    console.info('==> 💻  Send requests to http://%s:%s', config.apiHost, config.apiPort);
  });

  io.on('connection', (socket) => {
    socket.emit('news', {msg: `'Hello World!' from server`});

    socket.on('history', () => {
      for (let index = 0; index < bufferSize; index++) {
        const msgNo = (messageIndex + index) % bufferSize;
        const msg = messageBuffer[msgNo];
        if (msg) {
          socket.emit('msg', msg);
        }
      }
    });

    socket.on('msg', (data) => {
      data.id = messageIndex;
      messageBuffer[messageIndex % bufferSize] = data;
      messageIndex++;
      io.emit('msg', data);
    });
  });
  io.listen(runnable);
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
